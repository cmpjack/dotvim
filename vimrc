"""""""""""""""""""""""
"       PATHOGEN        "
"""""""""""""""""""""""""
execute pathogen#infect()
syntax on
filetype plugin on

set shiftwidth=2
set tabstop=2
set list
set expandtab
set number
set relativenumber
set laststatus=2
set t_8f=^[[38;2;%lu;%lu;%lum
set t_8b=^[[48;2;%lu;%lu;%lum
" set t_Co=256
set nobackup
set noswapfile

"""""""""""""""""""""""""
" Custom settings       "
"""""""""""""""""""""""""
source $HOME/.vim/startup/colorscheme.vim
source $HOME/.vim/startup/mappings.vim
source $HOME/.vim/startup/flagship.vim

highlight Normal ctermbg=NONE guibg=NONE
highlight NonText ctermbg=NONE guibg=NONE
highlight Visual cterm=NONE ctermbg=0  ctermfg=NONE guibg=Grey40

let delimitMate_expand_cr = 1

" Tagbar
nmap <F12> :TagbarToggle<CR>

" Syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_javascript_checkers=['eslint']
let g:syntastic_typescript_checkers = ['eslint']
highlight link SyntasticErrorSign SignColumn
highlight link SyntasticWarningSign SignColumn
highlight link SyntasticStyleErrorSign SignColumn
highlight link SyntasticStyleWarningSign SignColumn

" javascript indentation
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType vue setlocal shiftwidth=2 tabstop=2 expandtab
" autocmd FileType vue.html.javascript.css setlocal shiftwidth=2 tabstop=2 expandtab
" autocmd BufRead,BufNewFile *.vue setlocal filetype=vue.html.javascript.css

let g:airline_theme='base16_monokai'

let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:UltiSnipsExpandTrigger = "<C-j>"
let g:UltiSnipsJumpForwardTrigger = "<C-n>"
let g:UltiSnipsJumpBackwardTrigger = "<C-p>"
let g:UltiSnipsSnippetDirectories = ['~/.vim/UltiSnips', 'UltiSnips']
let g:vue_disable_pre_processors=1

:autocmd BufNewFile,BufRead /project/* vaxe#ProjectHxml("/project/project.hxml")
let g:vaxe_enable_airline_defaults = 0
:command! Bfly execute "!butterfly"

" Color line at 80 chars
" augroup vimrc_autocmds
"   autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#592929
"   autocmd BufEnter * match OverLength /\%80v.*/
" augroup END
set wrap
set colorcolumn=80

let NERDTreeShowHidden=1

" Show Syntax Group
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
